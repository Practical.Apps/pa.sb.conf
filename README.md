**Summary**

This is a bash script for configuring a linux system with a running PulseAudio sound server.\
The configuration allows playback of soundboard audio output while combining it with microphone audio input.

**Why**

Soundboard programs are great fun when interacting with friends over an internet voice chat.\
Linux soundboard and chat programs are available and easy to use, although using them together can prove difficult.\
PulseAudio is the sound server on most linux distros and it works well, but it is difficult for new users to understand.\
Researching PulseAudio for this script was a learning experience for me, adapting it yourself would be for you as well.\
Information on configuring PulseAudio in a similar way is available but very purpose specific.\
I have taken the time to appropriate this disparate information (see references) and combine them for a new purpose.\
I am happy to provide this work which will hopefully help you and others.

**How it works**

The script is thoroughly commented but I will provide a brief explanation here.\
PulseAudio can be controlled/configured on the fly with the pacmd and pactl commands.\
These commands can be used to create virtual devices and make connections between devices.

In this script, these commands are used as follows:\
-The script creates two virtual devices (sinks) and three loopback connections.\
-The soundboard audio output is fed into the first sink.\
-Audio from the first sink is looped back to the second sink and to the main audio out.\
-Audio from the microphone is looped back to the second sink.\
-Combined audio is taken from the second sink.

The end result is an H formation.

Speakers/Headset   Chat Program\
    ^          ^\
   Sink 1 ---------------------> Sink 2\
    ^          ^\
Soundboard Program  Microphone

With this configuration you have a single source for combined mic and soundboard audio.\
You are also able to hear the soundboard audio as it gets played which is missing from some configurations.\
The script frequently prompts the user for input as some steps may already have been taken.\
At the end of the script, all of the changed settings are reverted to their previous state.

**For your consideration**

*Customization*\
Unless you use the same headset as I do, you will likely need to adapt the script for your own.\
The sink and source names for your headset/mic can be found with 'pactl list short <sinks/sources>'.

Unless you use the same soundboard as I do, you will likely need to adapt the script for your own.\
Note in the script that the SB_NAME variable is not just for display, you may need to find it first.\
The soundboard name should appear on PulseAudio's clients list when audio is being played from it.\
You can pull up this list with 'pactl list short clients'

Depending on how you plan to use the script, you may want to cut out some of the options/questions.

*GUI Program*\
pacmd and pactl are standard with any PulseAudio install, they are command line only.\
pavucontrol (PulseAudio volume control), the gui interface usually is as well.\
paman (PulseAudio manager) is a GUI program to see more detailed information about your PulseAudio configuration.\
paman is not always included by default, if it interests you, you may want to install paman separately.

*Performance/CPU issues*\
Loopback modules create whole new streams of audio data.\
Without added latency these streams can be VERY processor intensive.\
I have included 10msec latency on all loopbacks, this can be changed but I recommend against lowering it.\
Even with these delays, the increase in CPU usage from PulseAudio after running the script is non-trivial.\
If you are on older hardware you should check this and consider the other programs you plan to run simultaneously.

*Default/Restore Devices*\
In the script the default sink can be changed (from speakers to headset, for example).\
In this section of the script all existing audio streams are changed to the new output.\
There is an idiosyncrasy of PulseAudio that affects programs started before this change.\
Even if they are closed and restarted, those programs will default to the previous audio output.\
This behavior can be changed in the 'module-stream-restore' section of the 'default.pa' config file.\
This is not necessary but may be helpful if you are the type who prefers to leave your computer running.\
See the last entry on the reference list for more information about this.\
I intentionally chose not to include any permanent changes in the script.

**References/Attribution**

The following pages were used for reference in the creation of this script:

The inspiration, knowledge of what is possible - Tactical_Tux\
https://www.reddit.com/r/linuxquestions/comments/cvvp81/are_there_any_good_soundboard_softwares_for_linux/ \
Expanded understanding, visualization - Emma Joe Anderson\
https://endless.ersoft.org/pulseaudio-loopback/ \
Very helpful search commands for extracting the relevant data in bash - dk75\
http://forums.debian.net/viewtopic.php?t=110440 \
Nuance of changing certain settings while a pulseaudio stream is running - Takkat, Gaco\
https://askubuntu.com/questions/71863/how-to-change-pulseaudio-sink-with-pacmd-set-default-sink-during-playback \
Nuance of default/fallback devices\
https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/DefaultDevice/

I'm providing attribution to these authors out of respect.\
These snippets of code were given and taken without concern for licensing.\
As such I provide this code to you unlicensed, but kindly request attribution if you reupload your own version.\
If you notice any mistakes or have any potential improvements, please leave an issue.
