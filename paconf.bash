#!/bin/bash
echo -n "Proceed with configuring PulseAudio for your soundboard?: "
read ans_on
if [ "$ans_on" = y ]
then
  DEFAULT_SOURCE=`pacmd dump | mawk '/set-default-source/ {print $2}'`
  DEFAULT_SINK=`pacmd dump | mawk '/set-default-sink/ {print $2}'`
  MIC_INPUT="alsa_input.usb-Cosair_Corsair_VOID_PRO_Surround_USB_Adapter_00000000-00.analog-stereo"
  MIC_OUTPUT="alsa_output.usb-Cosair_Corsair_VOID_PRO_Surround_USB_Adapter_00000000-00.analog-stereo"
  mod_array=(77777 77777 77777 77777 77777)
  # This function will get called twice if the user chooses to switch
  switch_sink () {
    # Change default output to the given sink
    pactl set-default-sink $1
    # Get the index number of the 
    IND=$(pactl list short sinks | grep "$1" | mawk '{print $1}')
    # Existing sink-inputs need to be changed, this pulls a list of ID#s
    pactl list short sink-inputs | mawk '{print $1}' | while read line
    do
      # Switch each sink-input to the given sink
      pacmd move-sink-input $line $IND
    done
  }

  echo "Loading modules..."

  # Create 2 virtual sinks and add their ID#s to the module array
  mod_array[0]=$(pactl load-module module-null-sink sink_name=SoundBoard1 sink_properties=device.description="Soundboard1")
  mod_array[1]=$(pactl load-module module-null-sink sink_name=SoundBoard2 sink_properties=device.description="Soundboard2")

  # Question user if they want to use headset output
  if [ $DEFAULT_SINK != $MIC_OUTPUT ]
  then
    echo -n "Audio output is not going to your headset, would you like to switch?: "
    read ans_out
    if [ "$ans_out" = y ]
    then
      FINAL_OUT=$MIC_OUTPUT
      switch_sink $MIC_OUTPUT
      echo "Output channel switched."
    else
      # User would have to change default output manually
      FINAL_OUT=$DEFAULT_SINK
      echo "Staying on default output channel."
    fi
  else
    FINAL_OUT=$DEFAULT_SINK
  fi

  # Create two loopbacks and add their ID#s to the module array
  mod_array[2]=$(pactl load-module module-loopback source=SoundBoard1.monitor sink=$FINAL_OUT latency_msec=10)
  mod_array[3]=$(pactl load-module module-loopback source=$MIC_INPUT sink=SoundBoard2 latency_msec=10)

  # Grab the last two loopback entries on the sink-inputs list (^^just made^^) and extract their ID#s and Sink ID#s to an array[4]
  snk_inp_arr=($(pactl list short sink-inputs | grep "loopback" | tail -n 2 | mawk '/module-loopback.c/ {print $1} {print $2}'))

  # Change the output of the SB1.monitor loopback from the default sink to SB2 (pactl move-sink-input ID SINK_ID)
  pactl move-sink-input ${snk_inp_arr[0]} ${snk_inp_arr[3]}

  # Add the first loopback again (The original was modified because regularloopback creation between sinks fails)
  mod_array[4]=$(pactl load-module module-loopback source=SoundBoard1.monitor sink=$FINAL_OUT latency_msec=10)

  # Keep predefined specs for Soundboard
  SB_NAME="CasterSoundboard" # This name is not just for show, it is specific to how it appears in the pactl client list
  SB_PATH="/home/owner/Downloads/CasterSoundboard/CasterSoundboard/CasterSoundboard" # Path to soundboard executable

  # Offer option to start soundboard program 
  echo -n "Start soundboard program ($SB_NAME)?: "
  read ans_start
  if [ "$ans_start" = y ]
  then
    $SB_PATH & # Amprisand needed to stop terminal from hanging
    SB_PID=$(echo $!)
    echo "$SB_NAME started. PID=$SB_PID"
  else
    # User would have to start soundboard program manually
    echo "Scripted start of $SB_NAME program canceled."
  fi

  # Prompt the user to queue up audio on the soundboard so it becomes available as a list entry
  echo "***Before proceeding with the next to the next step***"
  echo "Begin playing something in the soundboard and pause it."
  echo "That way $SB_NAME appears on the PA client list"

  # Assign soundboard client audio output to SB1
  echo -n "Change your soundboard output to SB1?: "
  read ans_opt
  if [ "$ans_opt" = y ]
  then
    # Check client list for the Soundboard program
    SB_CLNT_ID=$(pactl list short clients | grep "$SB_NAME" | mawk '{print $1}')
    echo "Client $SB_NAME found, sink ID: $SB_CLNT_ID"
    # Cancel early if no result is found
    if [ -z "$SB_CLNT_ID" ]
    then
      echo "Client $SB_NAME not found"
    else
      # Find entry in sink-inputs list corresponding to the soundboard client ID
      SB_CLNT_STRM_ID=$(pactl list short sink-inputs | mawk -v SBCID=${SB_CLNT_ID} '$3 == SBCID {print $1}')
      echo "$SB_NAME client stream ID: $SB_CLNT_STRM_ID"
      # Find the sink ID of SoundBoard1
      SB_SINK_ID=$(pactl list short sinks | grep "SoundBoard1" | mawk '{print $1}')
      # Change the output of the soundboard client to SoundBoard1
      pactl move-sink-input $SB_CLNT_STRM_ID $SB_SINK_ID
      echo "Soundboard output successfully changed"
    fi
  else
    # User would have to change soundboard program output manually
    echo "Soundboard configuration skipped"
  fi

  # Offer option to adjust SB1 Audio level
  echo -n "Set custom volume level for soundboard?: "
  read ans_cv
  if [ "$ans_cv" = y ]
  then
    echo -n "Enter volume as Int/150: "
    read ans_vol
    # Regular expression to check if entry was a number
    if ! [[ "$ans_vol" =~ ^[0-9]+$ ]]
      then 
        echo "error: Not a valid number, default will be 100%"
      else
        # Set volume to number entered (66000 is 100%, 99000/150% is max amplification)
        pactl set-source-volume  SoundBoard1.monitor $((ans_vol*660))
        echo "Soundboard1.monitor volume adjusted from 100 to $ans_vol %"
    fi
  else
    # User would have to change SoundBoard1 sink volume manually
    echo "No custom value set, default will be 100%"
  fi

  # Switch Default source to SB2
  pactl set-default-source  SoundBoard2.monitor

  printf -v joined '%s,' "${mod_array[@]}" # Join elements of the array together
  echo "Modules loaded: ${joined%,}" # Print comma separated list of modules

  # Prompt the user to undo changes
  # Kept within while loop in case of accidental user input
  ans_off=n
  while [ "$ans_off" != y ]
  do
    echo -n "Reverse changes?: "
    read ans_off
  done

  # Reset to default sink if it was changed
  if [ "$ans_out" = y ]
  then
    switch_sink $DEFAULT_SINK
    echo "Default audio output reset"
  fi

  # Reset default source
  pactl set-default-source $DEFAULT_SOURCE
  echo "Default audio input reset"

  # Cycle through module array and unload each module
  for each in "${mod_array[@]}"
  do
    echo "Unloading module: $each"
    pactl unload-module "$each"
  done
  echo "Modules unloaded"

  # Offer option to kill soundboard program
  if [ "$ans_start" = y ]
  then
    echo -n "Stop soundboard program ($SB_NAME)?: "
    read ans_stop
    if [ "$ans_stop" = y ]
    then
      kill $SB_PID
      echo "$SB_NAME stopped: PID $SB_PID killed"
    else
      echo "$SB_NAME program will be left open."
    fi
  fi

else
echo "PulseAudio configuration canceled"
fi
